% Matlab script to generate the json files for the reference cases
% Only the input tables are filled
%
% YC - 18.01.2019


% Starting reference case (~ Waltz standard case)
% Electrostatic, Miller circle, no collisions, no rotation, no impurities

% 1.a, 
ii=1;
out{ii}.ids_properties.provider='GKDB';
out{ii}.ids_properties.comment='GKDB reference case';

out{ii}.code=struct([]);

out{ii}.model.initial_value_run = true;
out{ii}.model.non_linear_run = false;
out{ii}.model.time_interval_norm = 'null';
out{ii}.model.include_a_field_parallel = false;
out{ii}.model.include_b_field_parallel = false;
out{ii}.model.include_full_curvature_drift = true;
out{ii}.model.include_centrifugal_effects = false;
out{ii}.model.collisions_pitch_only = false;
out{ii}.model.collisions_momentum_conservation = false;
out{ii}.model.collisions_energy_conservation = false;
out{ii}.model.collisions_finite_larmor_radius = false;

out{ii}.flux_surface.r_minor_norm = 0.15;
out{ii}.flux_surface.q = 2; 
out{ii}.flux_surface.magnetic_shear_r_minor = 1;
out{ii}.flux_surface.pressure_gradient_norm = 0;
out{ii}.flux_surface.b_field_tor_sign = 1;
out{ii}.flux_surface.ip_sign = 1;
out{ii}.flux_surface.shape_coefficients_c = [0.15 0 0 0 0 0 0 0];
out{ii}.flux_surface.shape_coefficients_s = [0 0 0 0 0 0 0 0];
out{ii}.flux_surface.dc_dr_minor_norm = [1 0 0 0 0 0 0 0];
out{ii}.flux_surface.ds_dr_minor_norm = [0 0 0 0 0 0 0 0];

out{ii}.species_all.velocity_tor_norm = 0;
out{ii}.species_all.shearing_rate_norm = 0;
out{ii}.species_all.debye_length_reference = 0;
out{ii}.species_all.beta_reference = 0;

% deuterium
out{ii}.species{1}.charge_norm = 1;
out{ii}.species{1}.mass_norm = 1;
out{ii}.species{1}.density_norm = 1;
out{ii}.species{1}.temperature_norm = 1;
out{ii}.species{1}.density_log_gradient_norm = 3;
out{ii}.species{1}.temperature_log_gradient_norm = 9;
out{ii}.species{1}.velocity_tor_gradient_norm = 0;

% electron
out{ii}.species{2}.charge_norm = -1;
out{ii}.species{2}.mass_norm = 2.724437108e-4;
out{ii}.species{2}.density_norm = 1;
out{ii}.species{2}.temperature_norm = 1;
out{ii}.species{2}.density_log_gradient_norm = 3;
out{ii}.species{2}.temperature_log_gradient_norm = 9;
out{ii}.species{2}.velocity_tor_gradient_norm = 0;

out{ii}.collisions.collisionality_norm=zeros(2,2);

ky=[0.1:0.1:1 1.5 2 4 6:6: 10 20 50 100];
nky=length(ky);
for jj=1:nky
  out{ii}.wavevector{jj}.poloidal_turns=11;
  out{ii}.wavevector{jj}.radial_component_norm=0;
  out{ii}.wavevector{jj}.binormal_component_norm=ky(jj);
  out{ii}.wavevector{jj}.eigenmode=[];
end
% to do: add a scan in kx for a few ky values

out{ii}.fluxes_integrated_norm=[];


flpth_json='~/projects/gkdb/gkdb_gitlab/json_files/reference_cases/';
flnm='ref_case_1';
writejson(out{ii},[flpth_json flnm '_' num2str(ii) '.json']);



return

[R,Z] = miller2rz(0.15,1.5,0,1,0,0,0,0,0,0,0,101); % R and Z in meters
[alpha,beta,alpha_pr,beta_pr,R0,Z0]=rz2genshape(R,Z,0.15,8);
