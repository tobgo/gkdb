% Matlab script to generate GKDB json file example(s)
% This script uses local folder information, and is pending removal
%
% YC - 10.04.2019


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% linear, initial value, with time dependence for third wavevector
if 0
% start from a real run and make the json file lighter
flnm='linexample';
proj='GKDB_TEST';

[out,is_ok,ok_msg]=gkw2json(flnm,proj,'~/tmp/'); 

out{1}.ids_properties.provider='GKDB';
out{1}.ids_properties.comment='This is a GKDB JSON example';
out{1}.code.name='test';
out{1}.code.parameters=[];
out{1}.code.parameters.code_specific_parameter_1=0.2;
out{1}.code.parameters.code_specific_parameter_2='flux-tube';
out{1}.code.parameters.code_specific_parameter_3=true;

out{1}.wavevector{3}.eigenmode{1}.time_norm=transpose([10 15 20]);
nt=length(out{1}.wavevector{3}.eigenmode{1}.time_norm);
out{1}.wavevector{3}.eigenmode{1}.phi_potential_perturbed_norm_real.data=repmat(out{1}.wavevector{3}.eigenmode{1}.phi_potential_perturbed_norm_real.data,1,nt);
out{1}.wavevector{3}.eigenmode{1}.phi_potential_perturbed_norm_imaginary.data=repmat(out{1}.wavevector{3}.eigenmode{1}.phi_potential_perturbed_norm_imaginary.data,1,nt);
out{1}.wavevector{3}.eigenmode{1}.a_field_parallel_perturbed_norm_real.data=repmat(out{1}.wavevector{3}.eigenmode{1}.a_field_parallel_perturbed_norm_real.data,1,nt);
out{1}.wavevector{3}.eigenmode{1}.a_field_parallel_perturbed_norm_imaginary.data=repmat(out{1}.wavevector{3}.eigenmode{1}.a_field_parallel_perturbed_norm_imaginary.data,1,nt);
out{1}.wavevector{3}.eigenmode{1}.b_field_parallel_perturbed_norm_real.data=repmat(out{1}.wavevector{3}.eigenmode{1}.b_field_parallel_perturbed_norm_real.data,1,nt);
out{1}.wavevector{3}.eigenmode{1}.b_field_parallel_perturbed_norm_imaginary.data=repmat(out{1}.wavevector{3}.eigenmode{1}.b_field_parallel_perturbed_norm_imaginary.data,1,nt);

moments=fieldnames(out{1}.wavevector{3}.eigenmode{1}.fluxes_moments{1}.moments_norm_rotating_frame);
for ii=1:length(moments)
 for jj=1:length(out{1}.wavevector{3}.eigenmode{1}.fluxes_moments)
  out{1}.wavevector{3}.eigenmode{1}.fluxes_moments{jj}.moments_norm_rotating_frame.(moments{ii}).data=repmat(out{1}.wavevector{3}.eigenmode{1}.fluxes_moments{jj}.moments_norm_rotating_frame.(moments{ii}).data,1,nt);
 end
end

flpth_json='~/projects/gkdb/gkdb_gitlab/gkdb/sample_files/';
flnm_json='linear_initialvalue';
keyboard
writejson(out{1},[flpth_json flnm_json '.json']);

% test the validity of the json file
% python3 -c "from gkdb.core.ids_checks import check_json; allow_entry=check_json('/home/yann/projects/gkdb/gkdb_gitlab/gkdb/sample_files/linear_initialvalue.json',only_input=0); print(allow_entry)"

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% linear, eigenvalue, 3 eigenvalues for the 2nd wavevector

if 1
% start from a real run and make the json file lighter
flnm='linexample';
proj='GKDB_TEST';

[out,is_ok,ok_msg]=gkw2json(flnm,proj,'~/tmp/'); 

out{1}.ids_properties.provider='GKDB';
out{1}.ids_properties.comment='This is a GKDB JSON example';
out{1}.code.name='test';
out{1}.code.parameters=[];
out{1}.code.parameters.code_specific_parameter_1=0.2;
out{1}.code.parameters.code_specific_parameter_2='flux-tube';
out{1}.code.parameters.code_specific_parameter_3=true;


out{1}.model.initial_value_run=false;
out{1}.wavevector{1}.eigenmode{1}.time_norm='null';
out{1}.wavevector{2}.eigenmode{1}.time_norm='null';
out{1}.wavevector{3}.eigenmode{1}.time_norm='null';

out{1}.wavevector{2}.eigenmode{2}=out{1}.wavevector{2}.eigenmode{1};
out{1}.wavevector{2}.eigenmode{3}=out{1}.wavevector{2}.eigenmode{1};

out{1}.wavevector{2}.eigenmode{2}.growth_rate_norm=0.2;
out{1}.wavevector{2}.eigenmode{2}.frequency_norm=-0.15;
out{1}.wavevector{2}.eigenmode{3}.growth_rate_norm=-0.05;
out{1}.wavevector{2}.eigenmode{3}.frequency_norm=1.2;

flpth_json='~/projects/gkdb/gkdb_gitlab/gkdb/sample_files/';
flnm_json='linear_eigenvalue';
keyboard
writejson(out{1},[flpth_json flnm_json '.json']);

% test the validity of the json file
% python3 -c "from gkdb.core.ids_checks import check_json; allow_entry=check_json('/home/yann/projects/gkdb/gkdb_gitlab/json_files/linear_eigenvalue.json',only_input=0); print(allow_entry)"
end
