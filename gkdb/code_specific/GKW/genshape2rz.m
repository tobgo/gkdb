% Compute the R,Z description of a flux surface from the GKDB parametrisation (and two adjacent surfaces for gradients computation)
% 
%   [R,Z] = genshape2rz(c,s,dcdr,dsdr,Nth)
% 
% Inputs:
%  c,s:          plasma shape coefficients (see GKDB manual)
%  dcdr, dsdr:   radial derivative of the plasma shape coefficients (see GKDB manual)
%  Nth:          number of poloidal points to discretize the flux surface (default: Nth=120)
%
% Outputs:
%  R,Z:          flux surfaces description (normalised to Rref with Zref=0)
%       
%

function [R,Z] = genshape2rz(c,s,dcdr,dsdr,Nth)

if ~exist('Nth')||isempty(Nth)
 Nth=120;
end

if ~isequal(size(c),size(s),size(dcdr),size(dsdr))
 disp('Shape coefficient arrays should all have the same size')
 return
end

if isvector(c)
 c=c(:); s=s(:); dcdr=dcdr(:); dsdr=dsdr(:);
else
 disp('Shape coefficients arrays should be vectors')
end
Nsh=length(c)-1;

th_grid = linspace(0,2*pi,Nth+1);
th_grid = th_grid(1:Nth);

Nr=3;
n=transpose([0:1:Nsh]);
THETA=repmat(th_grid,[Nsh+1 1]);
N=repmat(n,[1 Nth]);
C=repmat(c,[1 Nth]);
S=repmat(s,[1 Nth]);
DCDR=repmat(dcdr,[1 Nth]);
DSDR=repmat(dsdr,[1 Nth]);

% distance to the reference point
aN = sum(C.*cos(N.*THETA)+S.*sin(N.*THETA));
aNp = sum((C+DCDR.*2e-3).*cos(N.*THETA)+(S+DSDR.*2e-3).*sin(N.*THETA)); 
aNm = sum((C-DCDR.*2e-3).*cos(N.*THETA)+(S-DSDR.*2e-3).*sin(N.*THETA)); 

% R,Z
R=1+[aNm;aN;aNp].*cos(repmat(th_grid,[3 1]));
Z=-[aNm;aN;aNp].*sin(repmat(th_grid,[3 1]));

