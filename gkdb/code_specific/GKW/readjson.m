% Read a GKDB json file and convert it into a matlab structure
%
%   [out]=readjson(sss,sss_type);
% 
% Inputs:
%   sss       either 1) the path+name to read a json file
%                  2) a string with the content of a json file
%   sss_type  'file' (default) or 'string'
%
% Outputs: 
%   out     matlab structure with the content of the json file
%  
% Warning: this script is unlikely to properly read generic json files it is only meant to read those produced for the GKDB 
% Libraries to read generic json files are available in Python 
%
% YC - 21.01.2018


function [out]=readjson(sss,sss_type);

if ~exist('sss')
  disp('Input sss missing')
  return
end

if ~exist('sss_type')||isempty(sss_type)
  sss_type='file';
end

% read file if needed
if strcmp(sss_type,'file')&&unix(['test -e ' sss])==0
  sss=fileread(sss);
end


% find first block delimited by { }
[Istart, Iend]=find_block(sss,'{}');
sss_block=sss(Istart:Iend);


% within this block, find the various key/values
while ~isempty(sss_block(2:end-1))
  Itokens=regexp(sss_block,'"(\w+)":(?:.*?)("|{|[|\w|-)','tokenExtents','once'); % 1st token: field name, 2nd token: first character of the field content
  if ~isempty(Itokens)
    field_name=sss_block(Itokens(1,1):Itokens(1,2));
    str=sss_block(Itokens(2,1):end-1); % string starting after the field name and going to the end

    switch str(1)

     case '{' % new block
        [Istart,Iend]=find_block(str,'{}');     
        out.(field_name)=readjson(str(Istart:Iend),'string');
        sss_block=sss_block(Itokens(2,1)+Iend:end);
 
     case '"' % string
        Ivalue=regexp(str,'"(.*?)"(?:,\n|\n)','tokenExtents','once');         
        out.(field_name)=str(Ivalue(1):Ivalue(2));
        sss_block=sss_block(Itokens(2,1)+Ivalue(2)+1:end);

     case {'-','0','1','2','3','4','5','6','7','8','9'} % number
        Ivalue=regexp(str,'([+\-]?(?:(?:\d+\.\d*)|(?:\.\d+)|(?:\d+))(?:[eE][+\-]?\d+)?)(?:,\n|\n)','tokenExtents','once');     
        out.(field_name)=str2num(str(Ivalue(1):Ivalue(2)));
        sss_block=sss_block(Itokens(2,1)+Ivalue(2):end);

     case '[' % array
        [Istart,Iend]=find_block(str,'[]');  
        str_new=str(Istart+1:Iend-1); % string with the content of the array
        if isempty(str_new)
          out.(field_name)=[];   % empty table
        else
          if any(strfind(str_new,'{')) % array of structures
            ii=1;
            while ~isempty(deblank(str_new))
              [Istart_sub,Iend_sub]=find_block(str_new,'{}');
              str_new_block=str_new(Istart_sub:Iend_sub);   
              out.(field_name){ii}=readjson(str_new_block,'string');
              str_new=str_new(Iend_sub+1:end); 
              ii=ii+1;
            end
          else % standard array
            if strcmp(str_new(1),'[')
              str_new=regexprep(str_new,']\s*,\s*[','];['); % maximum dimension of the array is 2
              out.(field_name)=str2num(['[' str_new ']']);
            else 
             out.(field_name)=transpose(str2num(['[' str_new ']']));
            end
          end
        end
        sss_block=sss_block(Itokens(2,1)+Iend:end);

     case {'t','f'} %logical
        Ivalue=regexp(str,'(true|false)','tokenExtents','once');
        out.(field_name)=str2num(str(Ivalue(1):Ivalue(2)));
        sss_block=sss_block(Itokens(2,1)+Ivalue(2):end);

     case {'n'} %null
        Ivalue=regexp(str,'(null)','tokenExtents','once');
        out.(field_name)=str(Ivalue(1):Ivalue(2));
        sss_block=sss_block(Itokens(2,1)+Ivalue(2):end);

     end

  else
     if ~exist('out')
      out=struct([]);
     end
     break
  end
end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Istart, Iend]=find_block(sss_in,pattern)

if ~exist('pattern')|isempty(pattern)
 pattern='{}';
end

if ~(strcmp(pattern,'{}')|strcmp(pattern,'[]'))
 disp('Pattern not allowed. Use {} or []')
 return
end
I=regexp(sss_in,['(\' pattern(1) '|\' pattern(2) ')'],'start');

if ~strcmp(sss_in(I(1)),pattern(1)) | mod(length(I),2)>0
  disp('Non matching brackets. Check the json file?')
 return
end
ii=1;
n_open=1;
while ii<length(I) & n_open>0
  ii=ii+1;
  if strcmp(sss_in(I(ii)),pattern(1))
    n_open=n_open+1;
  else
    n_open=n_open-1;
  end
end
Istart=I(1);
Iend=I(ii); % sss_in(Istart:Iend) is the content of the first {xxx} block (including the curly braces)
