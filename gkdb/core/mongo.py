import json
import os
import getpass

from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, OperationFailure
from IPython import embed

def connect_to_mongo_gkdb():
    try:
        HOST = os.environ['MONGO_HOST']
    except KeyError:
        HOST = 'mongo.gkdb.org'
    try:
        DATABASE = os.environ['MONGO_DATABASE']
    except KeyError:
        DATABASE = 'gkdb'
    try:
        USER = os.environ['MONGO_USER']
    except KeyError:
        USER = input("username? ")
    try:
        PASS = os.environ['MONGO_PASSWORD']
    except KeyError:
        PASS = getpass.getpass()

    client = MongoClient(HOST, port=9000)
    try:
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
    except ConnectionFailure:
        raise Exception("Server not available")
    db = getattr(client, DATABASE)
    try:
        client.admin.authenticate(USER, PASS)
    except OperationFailure as ee:
        print('Could not authenticate user')
        raise
    return db

def upload_json(collection, json_path):
    with open(path, 'r') as file_:
        dict_ = json.load(file_)
        new_id = idsses.insert_one(dict_).inserted_id
    return new_id

if __name__ == '__main__':
    db = connect_to_mongo_gkdb()
    idsses = db.idsses

    path = os.path.abspath(os.path.join(__file__, '../../../json_files/linear_initialvalue.json'))
    # Uncomment this to upload something to the GKDB mongoDB-style
    #new_id = upload_json(idsses, path)
    #print('Inserted ID:', new_id)
