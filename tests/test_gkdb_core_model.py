import os
import json
from copy import deepcopy

from IPython import embed
import pytest
from dictdiffer import diff, patch, swap, revert

from conftest import test_files
from gkdb.core.model import *

db.init(database='gkdb_test', user='runner', password='runnerpass')
json_files_path = os.path.join(os.path.dirname(__file__), '../../../json_files')

class TestLoadExample():
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_load(self, test_file_name, json_structure):
        pass

class TestIds_properties():
    requires = [Ids_properties]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        Ids_properties.create(**example['ids_properties'])

class TestCode():
    requires = [Ids_properties, Code]
    database = db

    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        Code.create(ids_properties=ids,
                    **example['code']
        )

class TestModel():
    requires = [Ids_properties, Model]
    database = db

    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        Model.create(
            ids_properties=ids,
            **example['model']
        )

class TestFlux_surface():
    requires = [Ids_properties, Flux_surface]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        Flux_surface.create(
            ids_properties=ids,
            **example['flux_surface']
        )

class TestSpecies_all():
    requires = [Ids_properties, Species_all]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        Species_all.create(
            ids_properties=ids,
            **example['species_all']
        )

class TestWavevector():
    requires = [Ids_properties, Wavevector]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        Wavevector.create(
            ids_properties=ids,
            **example['wavevector'][0]
        )

class TestEigenmode():
    requires = [Ids_properties, Wavevector, Eigenmode]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        wv = Wavevector.create(
            ids_properties=ids,
            **example['wavevector'][0]
        )
        Eigenmode.create(
            wavevector=wv,
            **example['wavevector'][0]['eigenmode'][0]
        )

class TestSpecies():
    requires = [Ids_properties, Species]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        Species.create(
            ids_properties=ids,
            **example['species'][0]
        )


#class TestMoments_norm_rotating_frame():
#    requires = [Ids_properties, Species, Wavevector, Eigenmode, Moments_norm_rotating_frame]
#    database = db
#    def test_creation(self, setup_tables, load_default_example):
#        example = load_default_example
#        ids = Ids_properties.create(**example['ids_properties'])
#        wv = Wavevector.create(
#            ids_properties=ids,
#            **example['wavevector'][0]
#        )
#        em = Eigenmode.create(
#            wavevector=wv,
#            **example['wavevector'][0]['eigenmode'][0]
#        )
#        sp = Species.create(
#            ids_properties=ids,
#            **example['species'][0]
#        )
#        Moments_norm_rotating_frame.create(
#            species=sp,
#            eigenmode=em,
#            **example['moments_norm_rotating_frame']
#        )

class TestCollisions():
    requires = [Ids_properties, Species, Collisions]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        sp0 = Species.create(
            ids_properties=ids,
            **example['species'][0]
        )
        sp1 = Species.create(
            ids_properties=ids,
            **example['species'][1]
        )
        coll = example['collisions']['collisionality_norm']
        for ii, sp_first in enumerate([sp0, sp1]):
            for jj, sp_second in enumerate([sp0, sp1]):
                Collisions.create(species1_id=sp_first,
                                  species2_id=sp_second,
                                  collisionality_norm=coll[ii][jj])

class TestTag():
    requires = [Tag]
    database = db
    def test_creation(self, setup_tables):
        Tag.create(name='testtag', comment='hello')

class TestIds_properties_tag:
    requires = [Tag, Ids_properties, Ids_properties_tag]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        tag = Tag.create(name='testtag', comment='hello')
        ids = Ids_properties.create(**example['ids_properties'])
        Ids_properties_tag.create(ids_properties=ids, tag=tag)

class TestFluxes_norm:
    requires = [Ids_properties, Wavevector, Eigenmode, Species, Fluxes_norm]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    @pytest.mark.skip('Pending OMAS integarion')
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        ids = Ids_properties.create(**example['ids_properties'])
        wv = Wavevector.create(
            ids_properties=ids,
            **example['wavevector'][0]
        )
        eig = Eigenmode.create(
            wavevector=wv,
            **example['wavevector'][0]['eigenmode'][0]
        )
        sp0 = Species.create(
            ids_properties=ids,
            **example['species'][0]
        )
        Fluxes_norm.create(species=sp0, eigenmode=eig,
           **example['wavevector'][0]['eigenmode'][0]['fluxes_norm'][0],
        )

#class TestFluxes_integrated_norm:
#    requires = [Ids_properties, Species, Fluxes_integrated_norm]
#    database = db
#    def test_creation(self, setup_tables, load_default_example):
#        example = load_default_example
#        ids = Ids_properties.create(**example['ids_properties'])
#        sp0 = Species.create(
#            ids_properties=ids,
#            **example['species'][0]
#        )
#        Fluxes_integrated_norm.create(ids_properties=ids, species=sp0,
#           **example['fluxes_integrated_norm'][0]
#        )

#class TestMoments_norm_rotating_frame:
#    requires = [Ids_properties, Wavevector, Eigenmode, Species, Moments_norm_rotating_frame]
#    database = db
#    def test_creation(self, setup_tables, load_default_example):
#        example = load_default_example
#        ids = Ids_properties.create(**example['ids_properties'])
#        wv = Wavevector.create(
#            ids_properties=ids,
#            **example['wavevector'][0]
#        )
#        eig = Eigenmode.create(
#            wavevector=wv,
#            **example['wavevector'][0]['eigenmode'][0]
#        )
#        sp0 = Species.create(
#            ids_properties=ids,
#            **example['species'][0]
#        )
#        Fluxes_norm.create(species=sp0, eigenmode=eig,
#           **example['wavevector'][0]['eigenmode'][0]['moments_norm_rotating_frame'][0],
#        )

def compare_nested_dict(dict1, dict2):
    if isinstance(dict1, dict) and isinstance(dict2, dict):
        in1_not2 = set(dict1.keys()) - set(dict2.keys())
        in2_not1 = set(dict2.keys()) - set(dict1.keys())
        equality_dict = {}
        non_equality_dict = {}
        for key in set(dict2.keys()).intersection(set(dict1.keys())):
            if isinstance(dict1, dict) and isinstance(dict2, dict):
                res = compare_nested_dict(dict1[key], dict2[key])
                if not (res == True or res == (set(), set(), {})):
                    equality_dict[key] = res
                else:
                    print('what')
            else:
                if dict1[key] == dict2[key]:
                    equality_dict[key] = True
                else:
                    equality_dict[key] = False
        return in1_not2, in2_not1, equality_dict
    else:
        return True

class TestFullIds_properties():
    requires = [Ids_properties, Code, Model, Flux_surface, Species_all, Wavevector, Eigenmode, Species, Collisions, Fluxes_norm, Fluxes_integrated_norm, Moments_norm_rotating_frame]
    database = db
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        Ids_properties.from_dict(example)

    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        initial = deepcopy(example)
        ids = Ids_properties.from_dict(example)
        from_db = ids.to_dict()
        assert(list(diff(initial, from_db, tolerance=1e-5)) == [])

    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        del example['species']
        with pytest.raises(Exception):
            ids = Ids_properties.from_dict(example)

    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        # Delete electron
        for ii, spec in enumerate(example['species']):
            if spec['charge_norm'] == -1:
                elec_idx = ii
        del example['species'][elec_idx]
        with pytest.raises(Exception):
            ids = Ids_properties.from_dict(example)

    @pytest.mark.parametrize('test_file_name', test_files)
    def test_creation(self, setup_tables, test_file_name, json_structure):
        example = json_structure
        example['code']['name'] = 'illigal'
        with pytest.raises(Exception):
            ids = Ids_properties.from_dict(example)
