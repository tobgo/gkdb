import json
import os

from IPython import embed
import pytest

omas = pytest.importorskip("omas")
from conftest import test_files

class TestOmasCoupling():
    @pytest.mark.parametrize('test_file_name', test_files)
    def test_omas_loading(self, test_file_name, json_structure):
        ods = omas.ODS(consistency_check='warn')
        ods['gyrokinetics'].from_structure(json_structure)

    @pytest.mark.parametrize('test_file_name', test_files)
    def test_imas_compatibility(self, test_file_name, json_structure):
        ods = omas.ODS()
        ods['gyrokinetics'].from_structure(json_structure)
