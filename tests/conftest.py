import json
import os
import re

from IPython import embed
import pytest

test_files = [
    'gkdb/sample_files/linear_eigenvalue.json',
    'gkdb/sample_files/linear_initialvalue.json'
] # Common list of testfiles used in other tests

####
# General structure manipulation
####

def load_json_structure(json_path):
    with open(json_path, 'r') as f_:
        example = json.load(f_)
    return example

@pytest.fixture
def json_structure(request):
    """ Get the JSON structure from a parametrized test """
    calling_node_name = request.node.name
    search = re.search('\[(.+\.json)', calling_node_name)
    if search is not None:
        json_relpath = search.group(1)
    else:
        raise Exception('Could not parse json_relpath from {!s}'.format(calling_node_name))

    gkdb_root = str(request.config.rootdir)
    json_path = os.path.join(gkdb_root, json_relpath)
    return load_json_structure(json_path)

####
# SQL
####

@pytest.fixture()
def setup_tables(request):
    self = request.instance
    if self.requires:
        self.database.drop_tables(self.requires, safe=True, cascade=True)
        self.database.create_tables(self.requires)

    yield
    self.database.rollback()
    if self.requires:
        self.database.drop_tables(self.requires, safe=True, cascade=True)
